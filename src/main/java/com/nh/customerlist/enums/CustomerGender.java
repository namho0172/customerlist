package com.nh.customerlist.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerGender {
    MAN("남자"),
    WOMAN("여자");
    private final String name;
}
