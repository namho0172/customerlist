package com.nh.customerlist.enums;

import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerRating {
    RATING1("1등급"),
    RATING2("2등급"),
    RATING3("3등급"),
    RATING4("4등급");

    private final String name;
}
