package com.nh.customerlist.controller;

import com.nh.customerlist.model.CustomerItem;
import com.nh.customerlist.model.CustomerListRequest;
import com.nh.customerlist.model.CustomerResponse;
import com.nh.customerlist.service.CustomerListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerListController{
    private final CustomerListService customerListService;

    @PostMapping("/people")
    public String setCustomerList(@RequestBody CustomerListRequest request){
        customerListService.setCustomerList(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomer() {return customerListService.getCustomer();}

    @GetMapping("/detail/{id}")
    public CustomerResponse getCustomerResponse(@PathVariable long id) {
        return customerListService.getCustomerResponse(id);
    }
}
