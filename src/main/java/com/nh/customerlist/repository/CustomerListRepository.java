package com.nh.customerlist.repository;

import com.nh.customerlist.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerListRepository extends JpaRepository<Customer, Long> {
}
