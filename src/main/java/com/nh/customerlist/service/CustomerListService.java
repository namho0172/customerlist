package com.nh.customerlist.service;

import com.nh.customerlist.entity.Customer;
import com.nh.customerlist.model.CustomerItem;
import com.nh.customerlist.model.CustomerListRequest;
import com.nh.customerlist.model.CustomerResponse;
import com.nh.customerlist.repository.CustomerListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerListService {
    private final CustomerListRepository customerListRepository;

    public void setCustomerList(CustomerListRequest request) {
        Customer addData = new Customer();
        addData.setName(request.getName());
        addData.setRequestDate(LocalDate.now());
        addData.setReservationDate(LocalDate.now());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setCustomerGender(request.getCustomerGender());
        addData.setCustomerRating(request.getCustomerRating());

        customerListRepository.save(addData);
    }
    public List<CustomerItem> getCustomer(){//뭘 가지고 올지 선언하는 부분
        List<Customer> originList = customerListRepository.findAll();//원본데이터를 다 갖고오는 코드

        List<CustomerItem> result = new LinkedList<>();//묶는 끈

        for (Customer customer : originList){//원본 리스트에대해서 하나씩 갖고오는 부분
            CustomerItem addItem = new CustomerItem();//포 데이터를 갖고 오기 위해서는 빈그릇이 필요하기 때문에 만들어주는 부분
            addItem.setId(customer.getId());
            addItem.setName(customer.getName());
            addItem.setRequestDate(customer.getRequestDate());
            addItem.setReservationDate(customer.getReservationDate());
            addItem.setBirthDate(customer.getBirthDate());
            addItem.setPhoneNumber(customer.getPhoneNumber());
            addItem.setAddress(customer.getAddress());
            addItem.setCustomerGender(customer.getCustomerGender());
            addItem.setCustomerRating(customer.getCustomerRating());

            result.add(addItem);//result 가 끈이고 additem이 굴비 끈으로 굴비를 묶어버리는 선
        }
        return result;// 끈으로 묶은 굴비를 돌려주는 부분
    }
    public CustomerResponse getCustomerResponse(long id){//리스폰에 대해서 갖고오곘다는 선언
        Customer originDate = customerListRepository.findById(id).orElseThrow();//원본 데이터를 창고지기한테 갖고 오라고 시키는 중에 원하는 아이디 값이 있으면 갖고 오고 없으면 버린다는 선언

        CustomerResponse response = new CustomerResponse(); //굴비를 갖고 올 그릇을 만들어주는 선언
        response.setId(originDate.getId());
        response.setName(originDate.getName());
        response.setRequestDate(originDate.getRequestDate());
        response.setReservationDate(originDate.getReservationDate());
        response.setBirthDate(originDate.getBirthDate());
        response.setPhoneNumber(originDate.getPhoneNumber());
        response.setAddress(originDate.getAddress());
        response.setCustomerGender(originDate.getCustomerGender());
        response.setCustomerRating(originDate.getCustomerRating());

        return response;//reponse를 돌려준다

        // 제네릭과 상속 기능 이용해서 해결 해야한다.
    }
}
