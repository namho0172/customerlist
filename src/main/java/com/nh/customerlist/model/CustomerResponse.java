package com.nh.customerlist.model;

import com.nh.customerlist.enums.CustomerGender;
import com.nh.customerlist.enums.CustomerRating;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerResponse {
    private Long id;
    private String name;
    private LocalDate requestDate;
    private LocalDate reservationDate;
    private String birthDate;
    private String phoneNumber;
    private String address;
    private CustomerGender customerGender;
    private CustomerRating customerRating;
}
