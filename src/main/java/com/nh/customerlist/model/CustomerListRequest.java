package com.nh.customerlist.model;

import com.nh.customerlist.enums.CustomerGender;
import com.nh.customerlist.enums.CustomerRating;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerListRequest {
    private String name;
    private String birthDate;
    private String phoneNumber;
    private String address;

    @Enumerated(value = EnumType.ORDINAL)
    private CustomerGender customerGender;

    @Enumerated(value = EnumType.ORDINAL)
    private CustomerRating customerRating;
}
