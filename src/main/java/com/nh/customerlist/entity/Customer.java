package com.nh.customerlist.entity;

import com.nh.customerlist.enums.CustomerGender;
import com.nh.customerlist.enums.CustomerRating;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    private String name;

    @Column(nullable = false)
    private LocalDate requestDate;

    @Column(nullable = false)
    private LocalDate reservationDate;

    @Column(nullable = false, length = 15)
    private String birthDate;

    @Column(nullable = false,length = 15)
    private String phoneNumber;

    @Column(nullable = false,length = 40)
    private String address;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CustomerGender customerGender;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CustomerRating customerRating;
}
